# Meetup Parauapebas Tech Day

As apresentações estão disponíveis no diretório `presentations`.

## Build das Apresentações em AsciiDoc

Instalação do AsciiDoc no Fedora e Debian:

```
sudo {apt | dnf} install asciidoc
```

Instalar backjs backend:

```
wget https://github.com/houqp/asciidoc-deckjs/releases/download/v1.6.3/deckjs-1.6.3.zip
asciidoc --backend install deckjs-1.6.3.zip
```

Build das apresentações:

```
asciidoc -b deckjs meetup-pbs-tech-day.asciidoc
```

## Build com Docker

Com uma instalação funcional do Docker mais docker-compose basta rodar:

```
docker-compose up
```

Os slides estarão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
