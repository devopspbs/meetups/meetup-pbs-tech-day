#!/bin/sh

mkdir -p ./presentations

asciidoc --backend=deckjs \
         --out-file=presentations/meetup-pbs-tech-day.html \
         meetup-pbs-tech-day.asciidoc \
	 
exit 0
